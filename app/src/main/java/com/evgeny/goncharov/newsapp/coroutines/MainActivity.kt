package com.evgeny.goncharov.newsapp.coroutines

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import java.lang.ArithmeticException
import java.lang.Exception
import kotlin.coroutines.ContinuationInterceptor

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        GlobalScope.launch {
//            Log.d("MainActivity1", "start")
//            delay(1000)
//            Log.d("MainActivity1", "end")
//            Log.d("MainActivity1", Thread.currentThread().name)
//        }
//        Log.d("MainActivity1", "wow")

//        repeat(10) {
//            GlobalScope.launch {
//                Log.d("MainActivity1", "start $it is ${Thread.currentThread().name}")
//                delay(1000)
//                Log.d("MainActivity1", "end $it is ${Thread.currentThread().name}")
//            }
//        }
//        Log.d("MainActivity1", "wow")


//        GlobalScope.launch {
//            repeat(10) {
//                Log.d("MainActivity1", "start $it is ${Thread.currentThread().name}")
//                delay(1000)
//                Log.d("MainActivity1", "end $it is ${Thread.currentThread().name}")
//            }
//        }


        //чиолрен корутины
//        val job = GlobalScope.launch {
//            val job = coroutineContext[Job]
//            val coroutineName = coroutineContext[CoroutineName]
//            val dispatcher = coroutineContext[ContinuationInterceptor]
//            Log.d("MainActivity1", job.toString())
//
//            launch {
//                Log.d("MainActivity1", "captured")
//                delay(1000)
//                Log.d("MainActivity1", this.coroutineContext[Job].toString())
//            }
//
//
//            job?.children?.apply {
//                if(this.count() > 0) {
//                    this.forEach {
//                        Log.d("MainActivity1", it.toString())
//                    }
//                } else {
//                    Log.d("MainActivity1", "no children")
//                }
//            }
//        }


//        val job = GlobalScope.launch {
//            val job = coroutineContext[Job]
//            val coroutineName = coroutineContext[CoroutineName]
//            val dispatcher = coroutineContext[ContinuationInterceptor]
//            Log.d("MainActivity1", job.toString())
//
//            GlobalScope.launch {
//                Log.d("MainActivity1", "captured")
//                delay(1000)
//                Log.d("MainActivity1", this.coroutineContext[Job].toString())
//            }
//
//
//            job?.children?.apply {
//                if(this.count() > 0) {
//                    this.forEach {
//                        Log.d("MainActivity1", it.toString())
//                    }
//                } else {
//                    Log.d("MainActivity1", "no children")
//                }
//            }
//        }


//        val job = GlobalScope.launch {
//            val job = coroutineContext[Job]
//            val coroutineName = coroutineContext[CoroutineName]
//            val dispatcher = coroutineContext[ContinuationInterceptor]
//            Log.d("MainActivity1", job.toString())
//
//            supervisorScope {
//                Log.d("MainActivity1", "captured")
//                delay(1000)
//                Log.d("MainActivity1", this.coroutineContext[Job].toString())
//            }
//
//
//            job?.children?.apply {
//                if(this.count() > 0) {
//                    this.forEach {
//                        Log.d("MainActivity1", it.toString())
//                    }
//                } else {
//                    Log.d("MainActivity1", "no children")
//                }
//            }
//        }


//        GlobalScope.launch(
//            Dispatchers.IO +
//                    CoroutineExceptionHandler { coroutineContext, throwable ->
//
//                    } +
//                    Job() +
//                    CoroutineName("Hello World")
//        ) {
//
//        }

//        GlobalScope.launch {
//            try {
//                Log.d("MainActivity1", "1 " + Thread.currentThread().name)
//                withContext(Dispatchers.Main) {
//                    Log.d("MainActivity1", "2 " + Thread.currentThread().name)
//                }
//                Log.d("MainActivity1", "4 " + Thread.currentThread().name)
//                Log.d("MainActivity1", "done")
//            } catch (exc : Exception) {
//                Log.d("MainActivity1", exc.message)
//            }
//        }

//        GlobalScope.launch {
//            Log.d("MainActivity1", "1 " + Thread.currentThread().name)
//            withContext(Dispatchers.Unconfined) {
//                Log.d("MainActivity1", "2 " + Thread.currentThread().name)
//                delay(100)
//                foo()
//            }
//            Log.d("MainActivity1", "4 " + Thread.currentThread().name)
//            Log.d("MainActivity1", "done")
//        }


//        GlobalScope.launch(Dispatchers.Main) {
//            try {
//                Log.d("MainActivity1", Thread.currentThread().name)
//            } catch (exc: Exception) {
//                Log.d("MainActivity1", exc.message)
//            }
//        }


        //краш приложения
//        GlobalScope.launch {
//            launch {
//                throw Exception()
//            }
//        }

//        GlobalScope.launch {
//            val asyn = GlobalScope.async {
//                throw ArithmeticException()
//                0
//            }
//            //не вызовит эксепшен пока вы не вызовите await
////            val result = asyn.await()
//            try {
//                val result = asyn.await()
//            } catch (exc: Exception) {
//                Log.d("MainActivity1", "exception")
//            }
//        }


//        GlobalScope.launch(
//            CoroutineExceptionHandler { coroutineContext, throwable ->
//                //здесь сработает эксепшен
//                Log.d("MainActivity1", "exception")
//            }
//        ) {
//            throw Exception()
//        }

        //LAZY запуститься только после того как мы вызовим start
//        GlobalScope.launch {
//            val jobLazy = GlobalScope.launch(start = CoroutineStart.LAZY) {
//                Log.d("MainActivity1", "lazy start")
//            }
//            jobLazy.start()
//            delay(300)
//            Log.d("MainActivity1", "wow")
//        }

//        val j = GlobalScope.launch {
//            async()
//        }

        //supervisorScope доработает не смотря на то что у нас крашится чилдрен корутинв
//        GlobalScope.launch(CoroutineExceptionHandler { coroutineContext, throwable ->
//            Log.d("MainActivity1", "exception")
//        }) {
//            launch {
//                Log.d("MainActivity1", "start launch 1")
//                throw Exception()
//                Log.d("MainActivity1", "end launch 1")
//            }
//
//            supervisorScope {
//                Log.d("MainActivity1", "start launch 2")
//                Log.d("MainActivity1", "end launch 2")
//            }
//        }


    }

    var count = 0
    val mutex = Mutex()


    suspend fun async() {
        //синхронизация
        coroutineScope {
            repeat(100) {
                launch {
                    repeat(1000) {
                        action()
                    }
                }
            }
        }
        Log.d("MainActivity1", "$count")
    }


    private suspend fun action() {
        mutex.lock()
        count++
        mutex.unlock()
    }


    suspend fun foo() {
        withContext(Dispatchers.Unconfined) {
            Log.d("MainActivity1", "3 " + Thread.currentThread().name)
        }
    }


}
